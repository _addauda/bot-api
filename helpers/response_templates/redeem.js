const emoji = require('node-emoji');
const humanize = require('humanize-plus');

module.exports = () => {
	return {
		invalidCode: (code) => { return `Code ${code.toUpperCase()} is not valid. Please check code and try again ${emoji.get('thumbsup')}` },
		codeNotFound: (code) => { return `Code ${code.toUpperCase()} has no valid reward. Please check code and try again ${emoji.get('thumbsup')}` },
		notEnoughPoints: (points_diff) => { return `Customer does not have enough points to redeem reward - needs ${points_diff} ${humanize.pluralize((points_diff), "point")} ${emoji.get('astonished')}` },
		redeemReward: (code, reward_name) => { return `Code ${code.toUpperCase()} is valid for a ${reward_name} ${emoji.get('smile')}` }
	};
}