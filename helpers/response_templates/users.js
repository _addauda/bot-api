const emoji = require('node-emoji');
const humanize = require('humanize-plus');

const browse_stores_flow = process.env.MC_BROWSE_STORES_FLOW;
const check_points_flow = process.env.MC_CHECK_POINTS_FLOW;

const bot_base_url = process.env.PALLO_BOT_URL;

//const user_setup_add_email_flow = process.env.MC_USER_SETUP_ADD_EMAIL_FLOW;
//const user_setup_link_card_flow = process.env.MC_USER_SETUP_LINK_CARD_FLOW;

const link_card_flow = process.env.MC_LINK_CARD_FLOW;

const referral_dollar_amount = parseFloat(process.env.REFERRAL_DOLLAR_AMOUNT);

module.exports = () => {
	return {
		setManyChatMetaData: (_puid, first_name, user_meta) => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `${(first_name) ? `Sweet ${first_name}!` : "Okay" }, I've successfully created your account ${emoji.get('smiley')}`
						},
						{
							"type": "text",
							"text": `To finish the sign up process, you'll need to link your card. Type "link" to continue.`
						},
					],
					quick_replies: [
						{
							"type": "flow",
							"caption": `Link ${emoji.get('large_blue_circle')}`,
							"target": link_card_flow
						},
					],
					actions: [
						{
							action: "set_field_value",
							field_name: "_mcat",
							value: user_meta.many_chat_auth_token
						},
						{
							action: "set_field_value",
							field_name: "referral_code",
							value: user_meta.referral_code.toUpperCase()
						}
					]
				}
			}
		},

		setHasEmailField: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `You've successfully added an email to Pallo ${emoji.get('thumbsup')}. Try redeeming reward again.`,
						},
					],
					actions: [
						{
							action: "set_field_value",
							field_name: "hasEmail",
							value: "TRUE"
						}
					]
				}
			}
		},

		welcomeBack: (first_name) => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `${(first_name) ? `Welcome Back ${first_name}!` : "Welcome Back" } ${emoji.get('wave')}`
						},
						{
							"type": "text",
							"text": `What can I help you with? Type "points" to see your balance or "browse" to view restaurants.`
						},
					],
					quick_replies: [
						{
							"type": "flow",
							"caption": `Check My Points ${emoji.get('star2')}`,
							"target": check_points_flow
						},
						{
							"type": "flow",
							"caption": `Browse Stores ${emoji.get('convenience_store')}`,
							"target": browse_stores_flow
						},
					]
				}
			}
		},

		emailUpdateError: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Something went wrong while trying to add your email ${emoji.get('disappointed')}`
						},
						{
							"type": "text",
							"text": `Not to worry, help is on the way ${emoji.get('runner')}`
						},
					],
				}
			}
		},

		referralHasNoItems: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": "Ooops ... looks like you still need to complete the signup process. Type \"link\" to continue",
						},
					]
				}
			}
		},

		referralCodeNotFound: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": "Ooops ... I couldn't find any user with that code.",
						},
					]
				}
			}
		},

		referralCodeReflection: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": "Ooops ... You cannot redeem your own code.",
						},
					]
				}
			}
		},

		referralHasAppliedReferralCode: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": "Ooops ... You've already applied a referral code.",
						},
					]
				}
			}
		},

		referralPromptForMerchant: (referralMerchants, headers, body) => {
			let qr_elements = referralMerchants.map((element) => {
				return { 
						"type": "dynamic_block_callback",
						"caption": element.name,
						"url": `${bot_base_url}/points/referral/merchant/${element.id}`,
						"method": "get",
						"headers": {
							"_mcs": headers._mcs,
							"_mcat" : body.custom_fields_mcat,
							"_puid": body.id
					}
				}
			});

			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Awesome, You've now earned $${referral_dollar_amount} as your welcome bonus ${emoji.get('tada')}`
						},
						{
							"type": "text",
							"text": `Where would you like to apply these points?`,
						},
					],
					quick_replies: qr_elements
				}
			}
		},
		
	};
}