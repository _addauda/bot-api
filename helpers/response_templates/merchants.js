const emoji = require('node-emoji');
const humanize = require('humanize-plus');

const bot_base_url = process.env.PALLO_BOT_URL;

module.exports = () => {
	return {
		showMerchantCategories: (merchantCategoryArray, headers) => {
			let qr_elements = merchantCategoryArray.map((element) => {
				return { 
						"type": "dynamic_block_callback",
						"caption": element.name,
						"url": `${bot_base_url}/merchants/category/${element.id}`,
						"method": "get",
						"headers": {
							"_mcs": headers._mcs,
							"_mcat" : headers._mcat,
							"_puid": headers._puid
					}
				}
			});
		
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Select a category ${emoji.get('point_down')}`
						}
					],
					quick_replies: qr_elements
				}
			}
		},

		showMerchantsList: (merchantsArray, merchantCategoryArray, category_id, headers) => {
			
			let card_elements = merchantsArray.map((element) => {
				return { 
					"title": element.name,
					//"subtitle": "card text",
					"image_url": (element.image_url) ? element.image_url : "",
					"buttons": [
						{
							"type": "dynamic_block_callback",
							"caption": "See Rewards",
							"url": `${bot_base_url}/rewards/merchant/${element.id}`,
							"method": "get",
							"headers": { 
								"_mcs": headers._mcs,
								"_mcat" : headers._mcat,
								"_puid": headers._puid
							},
						}
					]
				}
			});
			
			let qr_elements = merchantCategoryArray.map((element) => {
				return { 
					"type": "dynamic_block_callback",
					"caption": (category_id && element.id == category_id) ? `${emoji.get('red_circle')} ${element.name}` : `${element.name}`,
					"url": `${bot_base_url}/merchants/category/${element.id}`,
					"method": "get",
					"headers": {
						"_mcs": headers._mcs,
						"_mcat" : headers._mcat,
						"_puid": headers._puid
					}
				}
			});

			return { 
				version: "v2",
				content: {
					messages: [
						{ 
							"type": "cards",
							"elements": card_elements,
							"image_aspect_ratio": "horizontal"
						}
					],
					quick_replies: qr_elements
				}
			}
		},

		noMerchantCategories: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": "No merchant or categories"
						},
					]
				}
			}
		}

	};
}