const emoji = require('node-emoji');
const humanize = require('humanize-plus');

const bot_base_url = process.env.PALLO_BOT_URL;

const mc_node_prefix = "check_points";

const browse_stores_flow = process.env.MC_BROWSE_STORES_FLOW;

module.exports = () => {
	return {

		showPointsSummary: (pointsSummaryArray, body, headers) => {

			let card_elements = pointsSummaryArray.map((element) => {
				return { 
					"title": element.merchant_name,
					"subtitle": ` You have ${emoji.get('star2')} ${humanize.formatNumber(element.points)} ${humanize.pluralize(element.points, "point")}`,
					"image_url": (element.image_url) ? element.image_url : "",
					"buttons": [
						{
							"type": "dynamic_block_callback",
							"caption": "See Rewards",
							"url": `${bot_base_url}/rewards/merchant/${element.merchant_id}`,
							"method": "get",
							"headers": { 
								"_mcs": headers._mcs,
								"_mcat": body.custom_fields._mcat,
								"_puid": body.id
							}
						}
					] 
				}
			});

			return { 
				version: "v2",
				content: {
					messages: [
						{ 
							"type": "cards",
							"elements": card_elements,
							"image_aspect_ratio": "horizontal"
						}
					]
				}
			}
		},

		// showPointsSummary: (pointsSummaryArray, body) => {
	
		// 	response = pointsSummaryArray.map((element, index) => {
		// 		return `${index+1}) ${element.merchant_name} - ${emoji.get('star2')}\u0020${humanize.formatNumber(element.points)} ${humanize.pluralize(element.points, "point")}`
		// 	}).join("\n");
		
		// 	return {
		// 		version: "v2",
		// 		content: {
		// 			messages: [ 
		// 				{
		// 					"type": "text",
		// 					"text": `${body.first_name}, here are the places where you have the most points ${emoji.get('point_down')}`
		// 				},
		// 				{
		// 					"type": "text",
		// 					"text": response
		// 				},
		// 			],
		// 			quick_replies: [
		// 				{
		// 					"type": "node",
		// 					"caption": `See Specific Store ${emoji.get('mag')}`,
		// 					"target": `${mc_node_prefix}_input_merchant`
		// 				},
		// 				{
		// 					"type": "flow",
		// 					"caption": `Browse Stores ${emoji.get('convenience_store')}`,
		// 					"target": browse_stores_flow
		// 				},
		// 			]
		// 		}
		// 	}
		// },

		showFuzzyMerchantOptions: (dataArray, body, headers) => {
			let qr_elements = dataArray.map((element) => {
				return { 
						"type": "dynamic_block_callback",
						"caption": element.name,
						"url": `${bot_base_url}/points/merchant/${element.id}`,
						"method": "get",
						"headers": {
							"_mcs": headers._mcs,
							"_mcat": headers._mcat,
							"_puid": headers._puid
					}
				}
			});
			return {
				version: "v2",
				content: { 
					messages: [ 
						{
							"type": "text",
							"text": `I'm sorry, I couldn't find any spot called ${body.custom_fields.merchant_context} ${emoji.get('disappointed')}. Did you mean any of these ${emoji.get('point_down')}?`,
						}
					],
					quick_replies: qr_elements
				}
			}
		},

		showPointsForMerchant: (pointsArray, headers, merchant_id) => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `You have ${humanize.formatNumber(pointsArray[0].points)} ${humanize.pluralize(pointsArray[0].points, "point")} at ${pointsArray[0].merchant_name} ${emoji.get('tada')}`,
							"buttons": [
								{
									"type": "dynamic_block_callback",
									"caption": "See Rewards",
									"url": `${bot_base_url}/rewards/merchant/${merchant_id}`,
									"method": "get",
									"headers": { 
										"_mcs": headers._mcs,
										"_mcat": headers._mcat,
										"_puid": headers._puid
									}
								}
							]
						},
					]
				}
			}
		},

		noPoints: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Oops … looks like you have no points here ${emoji.get('eyes')}`
						}
					],
					quick_replies: [
						{
							"type": "node",
							"caption": `New Search ${emoji.get('mag')}`,
							"target": `${mc_node_prefix}_input_merchant`
						},
						{
							"type": "flow",
							"caption": `Browse Stores ${emoji.get('convenience_store')}`,
							"target": browse_stores_flow
						},
					]
				}
			}
		},

		merchantNotFound: (body) => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Sorry ... I couldn't find any results for ${body.custom_fields.merchant_context}.`,
						},
					],
					quick_replies: [
						{
							"type": "node",
							"caption": `New Search ${emoji.get('mag')}`,
							"target": `${mc_node_prefix}_input_merchant`
						},
						{
							"type": "node",
							"caption": `See Summary ${emoji.get('scroll')}`,
							"target": `${mc_node_prefix}_show_summary`
						},
					]
				}
			}
		},

		updatedBalance: (pointsResult) => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Sweet! You now have ${emoji.get('star2')} ${humanize.formatNumber(pointsResult.points)} ${humanize.pluralize(pointsResult.points, "point")} at ${pointsResult.merchant_name}.`,
						},
					]
				}
			}
		},

		referralError: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Ooops ... I couldn't add to your points balance. Don't worry, human help is on the way ${emoji.get('thumbsup')}`,
						},
					]
				}
			}
		}
		
	};
}