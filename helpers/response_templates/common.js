const emoji = require('node-emoji');
const humanize = require('humanize-plus');

const link_card_flow = process.env.MC_LINK_CARD_FLOW;
const add_email_flow = process.env.MC_ADD_EMAIL_FLOW;

module.exports = () => {
	return {
		hasNoItems: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": "Ooops ... looks like you need to link a card to continue.",
							"buttons": [
								{
									"type": "flow",
									"caption": `Link My Card ${emoji.get('credit_card')}`,
									"target": link_card_flow
								}
							]
						},
					]
				}
			}
		},

		hasNoEmail: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": "Ooops ... looks like you need to add an email address to continue.",
							"buttons": [
								{
									"type": "flow",
									"caption": `Add My Email ${emoji.get('email')}`,
									"target": add_email_flow
								}
							]
						},
					]
				}
			}
		}
	
	};
}