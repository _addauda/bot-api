const emoji = require('node-emoji');
const humanize = require('humanize-plus');

const bot_base_url = process.env.PALLO_BOT_URL;

const mc_node_prefix = "browse_rewards";

const browse_stores_flow = process.env.MC_BROWSE_STORES_FLOW;

module.exports = () => {
	return {

		showRewardCards: (rewardsArray, pointsForMerchant, headers) => {

			let points_balance = (pointsForMerchant) ? pointsForMerchant : 0
			
			let card_elements = rewardsArray.map((element) => {
				return { 
					"title": `${element.name} - ${emoji.get('star2')} ${humanize.formatNumber(element.points)} ${humanize.pluralize(element.points, "point")}`,
					"subtitle": `${(points_balance < element.points) ? `You're ${humanize.formatNumber(element.points - points_balance)} ${humanize.pluralize(element.points - points_balance, "point")} away` : `You can redeem this reward ${emoji.get('tada')}` }`,
					"image_url": (element.image_url) ? element.image_url : "",
					"buttons": [
						{
							"type": "dynamic_block_callback",
							"caption": `${(points_balance >= element.points) ? `Redeem ${emoji.get('tada')}` : `Redeem` }`,
							"url": `${bot_base_url}/rewards/redeem/${element.id}/${element.type.toLowerCase()}`,
							"method": "get",
							"headers": { 
								"_mcs": headers._mcs,
								"_mcat": headers._mcat,
								"_puid": headers._puid
							}
						}
					] 
				}
			});

			return { 
				version: "v2",
				content: {
					messages: [
						{ 
							"type": "cards",
							"elements": card_elements,
							"image_aspect_ratio": "horizontal"
						},
						{
							"type": "text",
							"text": `${(points_balance) ? `Your points: ${emoji.get('star2')} ${humanize.formatNumber(points_balance)} ${humanize.pluralize(points_balance, "point")}` : `You have no points here ${emoji.get('disappointed')}`}`,
						}
					],
					quick_replies: [
						{
							"type": "flow",
							"caption": `New Search ${emoji.get('mag')}`,
							"target": browse_stores_flow
						}
					]
				}
			}
		},

		showFuzzyMerchantOptions: (dataArray, headers, body) => {
			let qr_elements = dataArray.map((element) => {
				return { 
						"type": "dynamic_block_callback",
						"caption": element.name,
						"url": `${bot_base_url}/rewards/merchant/${element.id}`,
						"method": "get",
						"headers": {
							"_mcs": headers._mcs,
							"_mcat" : headers._mcat,
							"_puid": headers._puid
					}
				}
			});
			return {
				version: "v2",
				content: { 
					messages: [ 
						{
							"type": "text",
							"text": `I'm sorry, I couldn't find any spot called ${body.custom_fields.merchant_context} ${emoji.get('disappointed')}. Did you mean any of these ${emoji.get('point_down')}?`,
						}
					],
					quick_replies: qr_elements
				}
			}
		},

		rewardNotFound: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": "No rewards"
						},
					]
				}
			}
		},

		showNewRewardCode: (reward_name, merchant_name, code) => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Booyah ${emoji.get('tada')}, here's your coupon code for your ${reward_name} ${emoji.get('point_right')} ${code.toUpperCase()} `
						},
						{
							"type": "text",
							"text": `Present it to your waiter or during checkout at ${merchant_name}` 
						},
					]
				}
			}
		},

		showPendingRewardCode: (code) => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `You have a redemption that is pending ${emoji.get('raised_hand')}. Here's the coupon code for your reward ${emoji.get('point_right')} ${code.toUpperCase()}`
						},
					]
				}
			}
		},

		showNewGiftCard: (reward_name, merchant_name, points) => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Awesome ${emoji.get('tada')}, you've redeemed a ${reward_name} at ${merchant_name}. I'll start working to deliver your gift card to your email ${emoji.get('fairy')}`
						},
						{
							"type": "text",
							"text": `Your new balance is ${points} ${humanize.pluralize((points), "point")}.`
						}
					]
				}
			}
		},

		showPendingGiftCard: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `You have a redemption that is pending ${emoji.get('raised_hand')}. I'm working hard to deliver your gift card to your email ${emoji.get('fairy')}`
						},
					]
				}
			}
		},

		noPoints: () => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Oops … You can't redeem this reward because you have no points here ${emoji.get('speak_no_evil')}`
						},
					]
				}
			}
		},
		
		notEnoughPoints: (points_diff) => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Oops … you need ${humanize.formatNumber(points_diff)} more ${humanize.pluralize(points_diff, "point")} to redeem this reward`
						},
					]
				}
			}
		},

		merchantNotFound: (body) => {
			return {
				version: "v2",
				content: {
					messages: [ 
						{
							"type": "text",
							"text": `Sorry ... I couldn't find any results for ${body.custom_fields.merchant_context}. You can try a new search or browse store categories ${emoji.get('ok_hand')}`,
						},
					],
					quick_replies: [
						{
							"type": "node",
							"caption": `New Search ${emoji.get('mag')}`,
							"target": `${mc_node_prefix}_input_merchant`
						},
						{
							"type": "node",
							"caption": `Browse Categories ${emoji.get('scroll')} `,
							"target": `${mc_node_prefix}_query_categories`
						},
					]
				}
			}
		}

	};
}