"use strict";
const boom = require('boom');
const axios = require('axios');
const instance = axios.create({
	baseURL: process.env.MC_API_URL,
	timeout: 1000,
	headers: {'Authorization': `Bearer ${process.env.MC_API_KEY}`}
});

const emoji = require('node-emoji');
const humanize = require('humanize-plus');

const bot_base_url = process.env.PALLO_BOT_URL;
const referral_dollar_amount = parseFloat(process.env.REFERRAL_DOLLAR_AMOUNT);

module.exports = (logger) => {
	return {
		sendRedeemSuccessMessage: async (_puid, reward_name, merchant_name, points) => {
			let bodyParameters = {
				subscriber_id: parseInt(_puid),
				data: {
					version: "v2",
					content: {
						messages: [ 
							{
								"type": "text",
								"text": `You've successfully redeemed a ${reward_name} at ${merchant_name} ${emoji.get('thumbsup')}`
							},
							{
								"type": "text",
								"text": `Your new balance is ${points} ${humanize.pluralize((points), "point")}.`
							}
						],
					}
				}
			}
			return instance.post(`/fb/sending/sendContent`, bodyParameters).catch((err) => { 
				logger.error(`Error occured whilst tring to use ManyChat API _puid:${_puid}`);
				//throw boom.internal("ManyChat API Error")
			} );
		},

		sendReferralSuccessMessage: async (referralMerchants, headers, user, reffered_user_name) => {
			let qr_elements = referralMerchants.map((element) => {
				return { 
						"type": "dynamic_block_callback",
						"caption": element.name,
						"url": `${bot_base_url}/points/referral/merchant/${element.id}`,
						"method": "get",
						"headers": {
							"_mcs": headers._mcs,
							"_mcat" : user.mcat,
							"_puid": user.id
					}
				}
			});


			let bodyParameters = {
				subscriber_id: parseInt(user.id),
				data: {
					version: "v2",
						content: {
						messages: [ 
							{
								"type": "text",
								"text": `${(reffered_user_name) ? reffered_user_name : "A friend"} just applied your referral code ${emoji.get('tada')}`
							},
							{
								"type": "text",
								"text": `You've now earned $${referral_dollar_amount}. Where would you like to apply these points?`
							}
						],
						quick_replies: qr_elements
					}
				}
			}
			return instance.post(`/fb/sending/sendContent`, bodyParameters).catch((err) => { 
				logger.error(err);
				console.log(err.response)
				logger.error(`Error occured whilst tring to use ManyChat API _puid:${user.id}`);
				//throw boom.internal("ManyChat API Error")
			} );
		}
	};
}