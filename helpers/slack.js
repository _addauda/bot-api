"use strict";
const boom = require('boom');

const axios = require('axios');

const slack_webhook_url = process.env.SLACK_WEBHOOK_URL;

module.exports = () => {
	return {
		sendFulfillmentNotification: (first_name, last_name, email, reward_name, redemption_id, merchant_name, code) => {
			return axios({
				method: 'post',
				url: slack_webhook_url,
				data: 
				{
					"blocks": [
	
						{
							"type": "section",
							"text": {
								"type": "mrkdwn",
								"text": "*New Giftcard Redemption*"
							}
						},
						{
							"type": "section",
							"fields": [
								{
									"type": "mrkdwn",
									"text": `*Recipient Name:*\n${first_name} ${last_name}`
								},
								{
									"type": "mrkdwn",
									"text": `*Reward:*\n${reward_name}`
								},
								{
									"type": "mrkdwn",
									"text": `*Code:*\n${code}`
								},
								{
									"type": "mrkdwn",
									"text": `*Merchant Name:*\n${merchant_name}`
								},
								{
									"type": "mrkdwn",
									"text": `*Recipient Email:*\n${email}`
								},
							]
						}
					]
					
				},
				headers: {contentType: "application/json"},
			}).catch((err) => { throw boom.internal(`${err.message}`)});

		},

	};
}
