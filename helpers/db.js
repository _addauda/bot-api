"use strict";
const format = require('pg-format');
const boom = require('boom');

module.exports = (pool) => {
	return {
		createUser: (user) => {
			let queryString = format("INSERT INTO users (id, first_name, last_name, gender) VALUES %L ON CONFLICT (id) DO NOTHING returning many_chat_auth_token, referral_code", [user])
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		hasEmail: (_puid) => {
			let queryString = format("SELECT 1 from users WHERE id = %L AND email is not null", _puid)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		hasItem: (_puid) => {
			let queryString = format("SELECT 1 from items WHERE user_id = %L and status = 'ACTIVE' LIMIT 1", _puid)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		hasAppliedReferralCode: (_puid) => {
			let queryString = format("SELECT 1 from referrals WHERE referred_user_id = %L LIMIT 1", _puid)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		addEmailToUser: (_puid, email) => {
			let queryString = format("UPDATE users SET email = %L WHERE id = %L", email, _puid)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		redeemCode: (id) => {
			let queryString = format("UPDATE redemption_codes SET status = 'REDEEMED' WHERE id = %L", id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getUser: (_puid) => {
			let queryString = format("SELECT * from users WHERE id = %L", _puid)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		createRedemptionCode: (reward_id, _puid) => {
			let queryString = format("INSERT INTO redemption_codes (reward_id, user_id) VALUES (%L, %L) returning code, id", reward_id, _puid)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getMerchantByName: (merchant_name) => {
			let queryString = format("SELECT * FROM merchants WHERE LOWER(name) = LOWER(%L) LIMIT 1", merchant_name)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getMerchantLikeName: (merchant_name) => {
			let queryString = format("SELECT * FROM merchants WHERE LOWER(%L) % LOWER(name) LIMIT 4", merchant_name)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getRewardsForMerchant: (merchant_id) => {
			let queryString = format("SELECT * FROM rewards WHERE merchant_id = %L order by points asc", merchant_id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getMerchantCategories: () => {
			let queryString = format("SELECT * FROM merchant_categories order by name asc LIMIT 11")
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getMerchantsForCategory: (category_id) => {
			let queryString = format("SELECT * FROM merchants WHERE merchant_category_id = %L LIMIT 10", category_id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getRewardStatus: (reward_id) => {
			let queryString = format("SELECT code FROM redemption_codes WHERE reward_id = %L AND status = 'PENDING'", reward_id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getRedemptionCode: (code) => {
			let queryString = format("SELECT id, user_id, reward_id FROM redemption_codes WHERE lower(code) = lower(%L) AND status = 'PENDING'", code)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},
		
		getPointsSummary: (_puid) => {
			let queryString = format("SELECT points_balance.points, merchants.id as merchant_id, merchants.name as merchant_name, merchants.image_url as image_url FROM points_balance join merchants on merchants.id = points_balance.merchant_id WHERE user_id = %L order by points_balance.points desc LIMIT 10", _puid)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getPointsForMerchantByRewardId: (_puid, reward_id) => {
			let queryString = format("select points_balance.points, \
				rewards.points as reward_points, merchants.name as merchant_name, \
				rewards.name as reward_name from points_balance \
				join merchants on merchants.id = points_balance.merchant_id \
				join rewards on points_balance.merchant_id = rewards.merchant_id  \
				where points_balance.user_id = %L and rewards.id = %L", _puid, reward_id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getPointsForMerchantByMerchantId: (_puid, merchant_id) => {
			let queryString = format("select points_balance.points, merchants.name as merchant_name from points_balance join merchants on merchants.id = points_balance.merchant_id where points_balance.user_id = %L and points_balance.merchant_id = %L", _puid, merchant_id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		getReferralCode: (referral_code) => {
			let queryString = format("SELECT id, many_chat_auth_token as mcat from users WHERE referral_code = LOWER(%L) LIMIT 1", referral_code)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},

		addToPointsBalance: (points, merchant_id, user_id) => {
			let queryString = format("INSERT INTO points_balance (points, merchant_id, user_id) \
				values (%L, %L, %L) ON CONFLICT (user_id, merchant_id) DO UPDATE SET points = points_balance.points + EXCLUDED.points, updated_at = now();", points, merchant_id, user_id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		},
		
		createReferral: (points, referring_user_id, referred_user_id) => {
			let queryString = format("INSERT INTO referrals (points, referring_user_id, referred_user_id) \
				values (%L, %L, %L)", points, referring_user_id, referred_user_id)
			return pool.query(queryString).catch((err) => { throw boom.internal(`${err.message}`)} );
		}

	};
}
