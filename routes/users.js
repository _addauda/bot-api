"use strict";

const express = require('express');
const router = express.Router();
const Validator = require('node-input-validator');
const boom = require('boom');

const responses = require('../helpers/response_templates/users')();

//formats full subscriber data into array
const formatUser = (_puid, first_name, last_name, gender) => {
	return [_puid, first_name, last_name, gender];
}

module.exports = (asyncMiddleware, dbHelpers, mcHelpers, logger) => {

	//create a new user and set _mcat
	router.post('/create', asyncMiddleware(async (req, res) => {
		
		console.log(req.body);
		let validator = new Validator(req.body, {
			'id': 'required|string',
			'first_name': 'string',
			'last_name': 'string',
			'gender': 'string'
		});

		let matched = await validator.check();
		if (!matched) {
			throw boom.badRequest(`input validation failed _puid:${req.body.id} with errors ${JSON.stringify(validator.errors)}`);
		}

		let user = formatUser(req.body.id, req.body.first_name, req.body.last_name, req.body.gender);

		let createUserResult = await dbHelpers.createUser(user);
		
		//if a new user is created
		if (createUserResult.rowCount) {

			logger.info(`created new user record _puid:${user[0]}`);

			//return response with _mcat set in subscriber field
			res.status(200).json(responses.setManyChatMetaData(req.body.id, req.body.first_name, createUserResult.rows[0]));
			return;
		}

		logger.warn(`could not create new user for _puid:${user[0]}`);
		//user already has an account - welcome back
		res.status(200).json(responses.welcomeBack(user[1]));
	}));

	//create a new user and set _mcat
	router.post('/addEmail', asyncMiddleware(async (req, res) => {
		
		let validator = new Validator(req.body, {
			'id': 'required|string',
			'custom_fields.email': 'required|string'
		});

		let matched = await validator.check();
		if (!matched) {
			throw boom.badRequest(`input validation failed _puid:${req.body.id} with errors ${JSON.stringify(validator.errors)}`);
		}

		let addEmailToUserResult = await dbHelpers.addEmailToUser(req.body.id, req.body.custom_fields.email);
		
		//if email was added successfully
		if (addEmailToUserResult.rowCount) {

			logger.info(`added email address to user _puid:${req.body.id}`);

			//return response with hasEmail set in custom field
			res.status(200).json(responses.setHasEmailField());
			return;
		}

		logger.warn(`could not update user email _puid:${req.body.id}`);
		//couldn't update email address
		res.status(200).json(responses.emailUpdateError());
	}));

	router.post('/getReferralCode', asyncMiddleware(async (req, res) => {
		
		let validator = new Validator(req.body, {
			'id': 'required|string'
		});

		let matched = await validator.check();
		if (!matched) {
			throw boom.badRequest(`input validation failed _puid:${req.body.id} with errors ${JSON.stringify(validator.errors)}`);
		}

		let getUserResult = await dbHelpers.getUser(req.body.id);
		
		//check to see if referral code is valid
		if (getUserResult.rowCount) {
			res.status(200).json({"referral_code": getUserResult.rows[0].referral_code});
		}

		logger.error(`could not find user _puid:${req.body.id}`);
		res.status(200).json({"error": "Not Found"});
	}));

	router.post('/referral', asyncMiddleware(async (req, res) => {
		
		let validator = new Validator(req.body, {
			'id': 'required|string'
		});

		let matched = await validator.check();
		if (!matched) {
			throw boom.badRequest(`input validation failed _puid:${req.body.id} with errors ${JSON.stringify(validator.errors)}`);
		}

		
		let hasItemResult = await dbHelpers.hasItem(req.body.id);

		//check to see if user has items
		if (!hasItemResult.rowCount) {
			logger.error(`user _puid:${req.body.id} with no items tried to redeem referral code: ${req.body.custom_fields.referral_code_context}`);
			res.status(200).json(responses.referralHasNoItems());
			return;
		}

		let hasAppliedReferralCodeResult = await dbHelpers.hasAppliedReferralCode(req.body.id);

		//check to see if user has already applied a refferal code, user can now only
		//earn referral points for referring new users
		if (hasAppliedReferralCodeResult.rowCount) {
			logger.error(`user _puid:${req.body.id} has already applied a referral code - trying to redeem referral code: ${req.body.custom_fields.referral_code_context}`);
			res.status(200).json(responses.referralHasAppliedReferralCode());
			return;
		}

		let getReferralCodeResult = await dbHelpers.getReferralCode(req.body.custom_fields.referral_code_context);
		
		//check to see if referral code is valid
		if (getReferralCodeResult.rowCount) {

			logger.info(`found user _puid:${getReferralCodeResult.rows[0].id} for referral code: ${req.body.custom_fields.referral_code_context}`);

			//check if user is trying to redeem their own code
			if (getReferralCodeResult.rows[0].id === req.body.id) {
				logger.error(`user _puid:${req.body.id} trying to redeem their own referral code`);
				res.status(200).json(responses.referralCodeReflection());
				return;
			}

			let createReferralResult = await dbHelpers.createReferral(parseInt(process.env.REFERRAL_POINTS), getReferralCodeResult.rows[0].id, req.body.id);
			logger.info(`created new referral record referrer: ${getReferralCodeResult.rows[0].id}, referred: ${req.body.id}`);
			
			let merchantsForReferral = [
				{	
					id: 1,
					name: "Starbucks"
				},
				{
					id: 2,
					name: "Tim Horton's"
				},
				{
					id: 4,
					name: "Demetres"
				},
				{
					id: 5,
					name: "Diary Queen"
				},
				{
					id: 7,
					name: "Subway"
				},
				{
					id: 35,
					name: "A&W"
				},
				{
					id: 19,
					name: "Jack Astor's"
				},
				{
					id: 31,
					name: "The Keg"
				},
				{
					id: 8,
					name: "Swiss Chalet"
				}
			]

			logger.info(`sending referral prompt for merchant response to referrer`);
			await mcHelpers.sendReferralSuccessMessage(merchantsForReferral, req.headers, getReferralCodeResult.rows[0], req.body.first_name)
			
			logger.info(`sending referral prompt for merchant response to referred`);
			res.status(200).json(responses.referralPromptForMerchant(merchantsForReferral,req.headers,req.body));
			return;
		}

		logger.error(`referral code not found code: ${req.body.custom_fields.referral_code_context} entered by user _puid:${req.body.id}`);
		res.status(200).json(responses.referralCodeNotFound());
	}));
	
	return router;
}