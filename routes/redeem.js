"use strict";

const express = require('express');
const router = express.Router();
const Validator = require('node-input-validator');
const boom = require('boom');

const MessagingResponse = require('twilio').twiml.MessagingResponse;

const twilio_secret = process.env.TWILIO_SECRET;

const responses = require('../helpers/response_templates/redeem')();

module.exports = (asyncMiddleware, dbHelpers, logger, slackHelpers, mcHelpers) => {

	router.post('/sms/incoming/token/:token', asyncMiddleware(async (req, res) => {
		
		const twiml = new MessagingResponse();

		let validator = new Validator(req.body, {
			'Body': 'required|alphaNumeric'
		});

		let matched = await validator.check();

		if (!matched || req.body.Body.length !== 4) {
			logger.error(`received sms code ${req.body.Body} with invalid format`);
			twiml.message(responses.invalidCode(req.body.Body));
			res.setHeader('content-type', 'text/xml');
			res.send(twiml.toString());
			return;
		}

		logger.info(`redirecting to validate endpoint for sms code ${req.body.Body}`);
		twiml.redirect({method: 'POST'},`../../validate/${req.body.Body}/token/${twilio_secret}`);
		res.send(twiml.toString());

	}));

	router.post('/sms/validate/:code/token/:token', asyncMiddleware(async (req, res) => {
		
		const twiml = new MessagingResponse();

		let validator = new Validator(req.params, {
			'code': 'required|alphaNumeric'
		});

		let matched = await validator.check();

		if (!matched || req.params.code.length !== 4) {
			logger.error(`received sms code ${req.params.code} with invalid format`);
			twiml.message(responses.invalidCode(req.params.code));
			res.setHeader('content-type', 'text/xml');
			res.send(twiml.toString());
			return;
		}

		logger.info(`checking redemption status for sms code ${req.params.code}`);
		let getCodeResult = await dbHelpers.getRedemptionCode(req.params.code);

		if (getCodeResult.rowCount) {
			
			logger.info(`checking points balance for user ${getCodeResult.rows[0].user_id} for sms code ${req.params.code}`);
			let getPointsResult = await dbHelpers.getPointsForMerchantByRewardId (getCodeResult.rows[0].user_id, getCodeResult.rows[0].reward_id);

			if (getPointsResult.rows[0].points >= getPointsResult.rows[0].reward_points) {

				logger.info(`setting code status to REDEEMED for sms code ${req.params.code}`);
				let redeemCodeResult = await dbHelpers.redeemCode(getCodeResult.rows[0].id);

				if (redeemCodeResult.rowCount) {

					logger.info(`sending sms response for redemption of sms code ${req.params.code}`);
					twiml.message(responses.redeemReward(req.params.code, getPointsResult.rows[0].reward_name));
					res.setHeader('content-type', 'text/xml');
					res.send(twiml.toString());

					logger.info(`sending message to chat for user ${getCodeResult.rows[0].user_id}`);
					mcHelpers.sendRedeemSuccessMessage(getCodeResult.rows[0].user_id, getPointsResult.rows[0].reward_name, getPointsResult.rows[0].merchant_name, (getPointsResult.rows[0].points - getPointsResult.rows[0].reward_points))
					return;
				}

			} else {
				logger.info(`user does not not have enough points for sms code ${req.params.code}`);
				twiml.message(responses.notEnoughPoints(getPointsResult.rows[0].reward_points - getPointsResult.rows[0].points));
				res.setHeader('content-type', 'text/xml');
				res.send(twiml.toString());
				return;
			}
		}

		logger.error(`could not find valid result for sms code ${req.params.code}`);
		twiml.message(responses.codeNotFound(req.params.code));
		res.setHeader('content-type', 'text/xml');
		res.send(twiml.toString());
		
	}));

	return router;
}