"use strict";

const express = require('express');
const router = express.Router();
const Validator = require('node-input-validator');
const boom = require('boom');

const bot_base_url = process.env.PALLO_BOT_URL;

const common_responses = require('../helpers/response_templates/common')();
const reward_responses = require('../helpers/response_templates/rewards')();

const mc_node_prefix = "browse_rewards";

module.exports = (asyncMiddleware, dbHelpers, logger, slack) => {

	router.post('/merchant/search', asyncMiddleware(async (req, res) => {

		let validator = new Validator(req.body, {
			'id': 'required|string',
			'custom_fields._mcat': 'required|string',
			'custom_fields.merchant_context': 'required|string',
		});

		let matched = await validator.check();
		if (!matched) {
			throw boom.badRequest(`input validation failed with errors ${JSON.stringify(validator.errors)}`);
		}

		// find merchant 
		let merchantByNameResult =  await dbHelpers.getMerchantByName(req.body.custom_fields.merchant_context);

		// merchant is found - return
		if (merchantByNameResult.rowCount) {
			logger.info(`found exact match for merchant: ${merchantByNameResult.rows[0].name} raw:${req.body.custom_fields.merchant_context}`);
			res.redirect(`${bot_base_url}/rewards/merchant/${merchantByNameResult.rows[0].id}`);
			return;
		}
		
		//perform fuzzy search to find merchant
		let merchantLikeNameResult =  await dbHelpers.getMerchantLikeName(req.body.custom_fields.merchant_context);

		//if fuzzy matches found
		if (merchantLikeNameResult.rowCount) {
			if (merchantLikeNameResult.rowCount > 1) { //multiple matches found - ask user to select options
				logger.info(`found ${merchantLikeNameResult.rows.length} fuzzy matches for raw:${req.body.custom_fields.merchant_context}`);
				res.status(200).json(reward_responses.showFuzzyMerchantOptions(merchantLikeNameResult.rows, req.headers, req.body));
				return;
			} else { //only one match found - confident it was what was requested
				logger.info(`found fuzzy match for merchant:${merchantLikeNameResult.rows[0].name} raw:${req.body.custom_fields.merchant_context}`);
				res.redirect(`${bot_base_url}/rewards/merchant/${merchantLikeNameResult.rows[0].id}`);
				return;
			}
		}
		
		//no fuzzy matches
		logger.error(`could not find match for raw:${req.body.custom_fields.merchant_context}`);
		res.status(200).json(reward_responses.merchantNotFound(req.body));

	}));

	router.get('/merchant/:merchant_id', asyncMiddleware(async (req, res) => {

		
		//retrieve all rewards for a merchant
		let rewardsResult = await dbHelpers.getRewardsForMerchant(req.params.merchant_id);

		//if merchant has rewards
		if (rewardsResult.rowCount) 
		{	
			let getPointsForMerchantResult = await dbHelpers.getPointsForMerchantByMerchantId(req.headers._puid, req.params.merchant_id);
			let pointsForMerchant = null;
			
			if (getPointsForMerchantResult.rowCount) {
				pointsForMerchant = getPointsForMerchantResult.rows[0].points;
			}

			logger.info(`found ${rewardsResult.rowCount} rewards at merchant:${req.params.merchant_id} for _puid: ${req.headers._puid}`);
			res.status(200).json(reward_responses.showRewardCards(rewardsResult.rows, pointsForMerchant, req.headers))
			return;
		}

		//no rewards found
		logger.info(`found ${rewardsResult.rowCount} rewards at merchant:${req.params.merchant_id} for _puid: ${req.headers._puid}`);
		res.status(200).json(reward_responses.rewardNotFound());	

	}));

	router.get('/redeem/:reward_id/:reward_type', asyncMiddleware(async (req, res) => {

		let hasItemResult = await dbHelpers.hasItem(req.headers._puid);

		if (!hasItemResult.rowCount) {
			logger.error(`user has not linked a card _puid:${req.headers._puid}`);
			res.status(200).json(common_responses.hasNoItems());
			return;
		} 

		//if the redemption type is an egiftcard - check if the user has email
		if (req.params.reward_type === "egiftcard") {
			let hasEmailResult = await dbHelpers.hasEmail(req.headers._puid);
			if (!hasEmailResult.rowCount) {
				logger.error(`user has not added an email _puid:${req.headers._puid}`);
				res.status(200).json(common_responses.hasNoEmail());
				return;
			}
		} 

		let getRewardStatusResult = await dbHelpers.getRewardStatus(parseInt(req.params.reward_id));
		
		//if user has an pending code for the same reward id - return the same code
		if (getRewardStatusResult.rowCount) {

			if (req.params.reward_type === "egiftcard") {
				logger.info(`egiftcard status still pending reward_id:${req.params.reward_id}`);
				res.status(200).json(reward_responses.showPendingGiftCard());
				return;
			} else {
				logger.info(`user had a pending code for reward_id:${req.params.reward_id}`);
				res.status(200).json(reward_responses.showPendingRewardCode(getRewardStatusResult.rows[0].code));
				return;
			}
		}

		//retrieve point balance for user
		let getPointsForMerchantByRewardResult = await dbHelpers.getPointsForMerchantByRewardId(req.headers._puid, parseInt(req.params.reward_id));

		//if user has points at the merchant
		if (getPointsForMerchantByRewardResult.rowCount) {
			
			//if the number of points the user has is enough to redeem reward - create and return new code
			if (getPointsForMerchantByRewardResult.rows[0].points >= getPointsForMerchantByRewardResult.rows[0].reward_points) {

				let createRedemptionCodeResult = await dbHelpers.createRedemptionCode(parseInt(req.params.reward_id), req.headers._puid);

				if (req.params.reward_type === "egiftcard") {
					logger.info(`created new giftcard reward_id:${req.params.reward_id}`);
					
					//redeem reward upon creation
					let redeemCodeResult = await dbHelpers.redeemCode(createRedemptionCodeResult.rows[0].id);

					res.status(200).json(reward_responses.showNewGiftCard(getPointsForMerchantByRewardResult.rows[0].reward_name, getPointsForMerchantByRewardResult.rows[0].merchant_name, (getPointsForMerchantByRewardResult.rows[0].points - getPointsForMerchantByRewardResult.rows[0].reward_points)));

					//send notification to slack
					let getUserResult = await dbHelpers.getUser(req.headers._puid);
					let sendFulfillmentNotificationResult = await slack.sendFulfillmentNotification(getUserResult.rows[0].first_name, getUserResult.rows[0].last_name, getUserResult.rows[0].email,
						getPointsForMerchantByRewardResult.rows[0].reward_name, createRedemptionCodeResult.rows[0].id, getPointsForMerchantByRewardResult.rows[0].merchant_name, createRedemptionCodeResult.rows[0].code);
					return;
				} else {
					logger.info(`created new code for reward_id:${req.params.reward_id}`);
					res.status(200).json(reward_responses.showNewRewardCode(getPointsForMerchantByRewardResult.rows[0].reward_name, getPointsForMerchantByRewardResult.rows[0].merchant_name, createRedemptionCodeResult.rows[0].code));
					return;
				}
			} else { //user does not have enough points
				logger.info(`user does have enough points - has:${getPointsForMerchantByRewardResult.rows[0].points} needs:${getPointsForMerchantByRewardResult.rows[0].reward_points} for reward_id:${req.params.reward_id}`);
				res.status(200).json(reward_responses.notEnoughPoints((getPointsForMerchantByRewardResult.rows[0].reward_points - getPointsForMerchantByRewardResult.rows[0].points)));
				return;
			}
		}

		//user has no points at merchant
		logger.info(`user has no points for reward_id:${req.params.reward_id}`);
		res.status(200).json(reward_responses.noPoints());

	}));

	return router;
}
