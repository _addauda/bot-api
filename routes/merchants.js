"use strict";

const express = require('express');
const router = express.Router();
const Validator = require('node-input-validator');
const boom = require('boom');

const responses = require('../helpers/response_templates/merchants')();

module.exports = (asyncMiddleware, dbHelpers, logger) => {

	//get all merchant categories
	router.get('/categories', asyncMiddleware(async (req, res) => {

		let getMerchantCategoriesResult = await dbHelpers.getMerchantCategories();

		if (getMerchantCategoriesResult.rowCount) {
			logger.info(`retrieved ${getMerchantCategoriesResult.rowCount} categories`);
			res.status(200).json(responses.showMerchantCategories(getMerchantCategoriesResult.rows, req.headers));
			return;		
		}
		
		logger.info(`no merchant categories`);
		res.status(200).json(responses.noMerchantCategories());

	}));

	//get all merchants belonging to a category
	router.get('/category/:category_id', asyncMiddleware(async (req, res) => {

		let getMerchantsResult = await dbHelpers.getMerchantsForCategory(req.params.category_id);
		let getMerchantCategoriesResult = await dbHelpers.getMerchantCategories();

		if (getMerchantsResult.rowCount && getMerchantCategoriesResult.rowCount) {
			logger.info(`retrieved ${getMerchantsResult.rowCount} merchants for category_id:${req.params.category_id}`);
			res.status(200).json(responses.showMerchantsList(getMerchantsResult.rows, getMerchantCategoriesResult.rows, req.params.category_id, req.headers));
			return;
		}	
		
		logger.info(`no merchants for category_id:${req.params.category_id}`);
		res.status(200).json(responses.noMerchantCategories());

	}));

	return router;
}