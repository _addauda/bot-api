"use strict";

const express = require('express');
const router = express.Router();
const Validator = require('node-input-validator');
const boom = require('boom');

const bot_base_url = process.env.PALLO_BOT_URL;

const common_responses = require('../helpers/response_templates/common')();
const points_responses = require('../helpers/response_templates/points')();

module.exports = (asyncMiddleware, dbHelpers, logger) => {

	router.post('/summary', asyncMiddleware(async (req, res) => {
		let validator = new Validator(req.body, {
			'id': 'required|string',
			'custom_fields._mcat': 'required|string'
		});

		let hasItemResult = await dbHelpers.hasItem(req.body.id);

		if (!hasItemResult.rowCount) {
			res.status(200).json(common_responses.hasNoItems());
			return;
		}

		let matched = await validator.check();
		if (!matched) {
			throw boom.badRequest(`input validation failed with errors ${JSON.stringify(validator.errors)}`);
		}

		let pointsSummaryResult = await dbHelpers.getPointsSummary(req.body.id);

		if (pointsSummaryResult.rowCount) {
			res.status(200).json(points_responses.showPointsSummary(pointsSummaryResult.rows, req.body, req.headers));
			logger.info(`completed points summary request for _puid: ${req.body.id}`);
			return;
		} 

		res.status(200).json(points_responses.noPoints());
		logger.info(`user has no points for _puid: ${req.body.id}`);
	}));

	router.post('/merchant/search', asyncMiddleware(async (req, res) => {

		let validator = new Validator(req.body, {
			'id': 'required|string',
			'custom_fields._mcat': 'required|string',
			'custom_fields.merchant_context': 'required|string',
		});

		let matched = await validator.check();
		if (!matched) {
			throw boom.badRequest(`input validation failed with errors ${JSON.stringify(validator.errors)}`);
		}

		let hasItemResult = await dbHelpers.hasItem(req.body.id);

		if (!hasItemResult.rowCount) {
			logger.error(`user has not linked a card _puid:${req.headers._puid}`);
			res.status(200).json(common_responses.hasNoItems());
			return;
		}

		// find merchant 
		let merchantByNameResult =  await dbHelpers.getMerchantByName(req.body.custom_fields.merchant_context);

		// merchant is found - return
		if (merchantByNameResult.rowCount) {
			logger.info(`found exact match for merchant: ${merchantByNameResult.rows[0].name} raw:${req.body.custom_fields.merchant_context}`);
			res.redirect(`${bot_base_url}/points/merchant/${merchantByNameResult.rows[0].id}`);
			return;
		}
		
		// perform fuzzy search to find merchant
		let merchantLikeNameResult =  await dbHelpers.getMerchantLikeName(req.body.custom_fields.merchant_context);

		//if fuzzy matches found
		if (merchantLikeNameResult.rowCount) {
			if (merchantLikeNameResult.rowCount > 1) {// multiple matches found - ask user to select options
				logger.info(`found ${merchantLikeNameResult.rows.length} fuzzy matches for raw:${req.body.custom_fields.merchant_context}`);
				res.status(200).json(points_responses.showFuzzyMerchantOptions(merchantLikeNameResult.rows, req.body, req.headers));
				return;
			} else { //only one match found - confident it was what was requested
				logger.info(`found fuzzy match for merchant:${merchantLikeNameResult.rows[0].name} raw:${req.body.custom_fields.merchant_context}`);
				res.redirect(`${bot_base_url}/points/merchant/${merchantLikeNameResult.rows[0].id}`);
				return;
			}
		} 
		
		//no fuzzy matches
		logger.error(`could not find match for raw:${req.body.custom_fields.merchant_context}`);
		res.status(200).json(points_responses.merchantNotFound(req.body));

	}));

	router.get('/merchant/:merchant_id', asyncMiddleware(async (req, res) => {
		
		//retrieve all points a user has for a merchant
		let pointsForMerchantResult = await dbHelpers.getPointsForMerchantByMerchantId(req.headers._puid, req.params.merchant_id);
		
		//if user has points
		if (pointsForMerchantResult.rowCount) {
			logger.info(`completed points at merchant:${req.params.merchant_id} request for _puid: ${req.headers._puid}`);
			res.status(200).json(points_responses.showPointsForMerchant(pointsForMerchantResult.rows, req.headers, req.params.merchant_id));
			return;
		} 
		
		//no points
		logger.info(`user has no points at merchant:${req.params.merchant_id} _puid: ${req.headers._puid}`);
		res.status(200).json(points_responses.noPoints());

	}));



	router.get('/referral/merchant/:merchant_id', asyncMiddleware(async (req, res) => {
		
		let referral_points = parseInt(process.env.REFERRAL_POINTS);

		//retrieve all points a user has for a merchant
		let addToPointsBalanceResult = await dbHelpers.addToPointsBalance(referral_points, req.params.merchant_id, req.headers._puid);
		logger.info(`Added ${referral_points} for user:${req.headers._puid} at merchant:${req.params.merchant_id}`);
		
		let getPointsResult = await dbHelpers.getPointsForMerchantByMerchantId(req.headers._puid,req.params.merchant_id);

		if (getPointsResult.rows) {
			logger.info(`completed points at merchant:${req.params.merchant_id} request for _puid: ${req.headers._puid}`);
			res.status(200).json(points_responses.updatedBalance(getPointsResult.rows[0]));
			return;
		}
		
		//no points
		logger.info(`could not apply point at merchant:${req.params.merchant_id} for _puid: ${req.headers._puid}`);
		res.status(200).json(points_responses.referralError());

	}));

	return router;
}