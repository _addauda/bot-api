'use strict';
require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const boom = require('boom');

const Pool = require('pg').Pool
const pool = new Pool({
	host: process.env.DB_HOST,
	database: process.env.DB_NAME,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	port: process.env.DB_PORT,
});

//application logger for app events
const log4js = require('log4js');
log4js.configure(
	{
		appenders: {
			file: {
				type: 'file',
				filename: `app.log`,
				maxLogSize: 10 * 1024 * 1024, // = 10Mb
				backups: 5, // keep five backup files
				compress: true, // compress the backups
				encoding: 'utf-8',
				mode: 0o0640,
				flags: 'w+'
			},
			out: {
				type: 'stdout'
			}
		},
		categories: {
			default: { 
				appenders: ['file', 'out'],
				level: 'trace' 
			}
		}
	}
);

const logger = log4js.getLogger();

//app constants
const PORT = process.env.PORT;
const HOST = process.env.HOST;

const obfuscate = (urlToObfuscate) => {
	return urlToObfuscate.replace(/[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i,  '$REDACTED$');
};

//app
const app = express();

//network logger app requests
app.use(morgan((tokens, request, response) => {
		return [
			tokens['remote-addr'](request, response), '-', '-',
			tokens['remote-user'](request, response,), '[',
			tokens['date'](request, response), ']', 
			'"', tokens.method(request, response),
			obfuscate(tokens.url(request, response)), 'HTTP/',
			tokens['http-version'](request, response), '"',
			tokens.status(request, response),
			tokens.res(request, response, 'content-length'), '-',
			tokens['response-time'](request, response), 'ms'
		].join(' ')
	}, {
		stream: fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
	}

));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//custom auth validator for many chat secret
app.use((req, res, next) => {
	if (req.path === "/") { //allow access to home route for health checks from e.g ALB
		next();
	} else if (req.path.startsWith("/bot") && req.headers._mcs && req.headers._mcs === process.env.MANY_CHAT_SECRET) {
		next();
	} else if (req.path.startsWith("/redeem") && req.path.includes(process.env.TWILIO_SECRET)) {
		next();
	} else {
		logger.error(`Bad Auth Credentials for path: ${obfuscate(req.path)}`);
		return res.sendStatus(401);
	}	
})

//async middleware for uniform error handling
const asyncMiddleware = fn => (req, res, next) => {
	Promise.resolve(fn(req, res, next)).catch((err) => {
		if (!err.isBoom) {
			err = boom.badImplementation(err);
			res.sendStatus(err.output.payload.statusCode);
			next(err);
		}
		logger.error(err.output.payload.statusCode, err.output.payload.message);
		res.sendStatus(err.output.payload.statusCode);
		next(err);
	});
};

const dbHelpers = require("./helpers/db")(pool);
const mcHelpers = require("./helpers/mc.js")(logger);
const slackHelpers = require("./helpers/slack")();

//routes
const usersRoutes = require("./routes/users")(asyncMiddleware, dbHelpers, mcHelpers, logger);
const pointsRoutes = require("./routes/points")(asyncMiddleware, dbHelpers, logger);
const merchantsRoutes = require("./routes/merchants")(asyncMiddleware, dbHelpers, logger);
const rewardsRoutes = require("./routes/rewards")(asyncMiddleware, dbHelpers, logger, slackHelpers);
const redeemRoutes = require("./routes/redeem")(asyncMiddleware, dbHelpers, logger, slackHelpers, mcHelpers);

//mount routes
app.use("/bot/users", usersRoutes);
app.use("/bot/points", pointsRoutes);
app.use("/bot/merchants", merchantsRoutes);
app.use("/bot/rewards", rewardsRoutes);
app.use("/redeem", redeemRoutes);

//default endpoint
app.get('/', (req, res) => {
	res.status(200).send({status:"Success", message:"Endpoint is up"});
});

app.listen(PORT, HOST);
logger.info(`${process.env.SERVICE_NAME} is running on http://${HOST}:${PORT}`);